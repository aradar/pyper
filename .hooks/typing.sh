#!/usr/bin/env bash

set -e

echo "calling 'mypy --strict --package pyper'"
echo "======================================="
mypy --strict --package pyper --strict
echo

echo "calling 'mypy --strict examples/'"
echo "================================="
mypy --strict examples/
