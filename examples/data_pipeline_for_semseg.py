import functools
import os
from pathlib import Path
from typing import Dict, Iterable, Generator, Iterator

import numpy as np
import numpy.typing as npt
import tensorflow as tf
import tensorflow_datasets as tfds
from pywuffs import ImageDecoderType, PixelFormat
from pywuffs.aux import ImageDecoderConfig, ImageDecoder
from typing_extensions import TypeAlias

from pyper import Pyper


TARGET_WIDTH = 1400
TARGET_HEIGHT = 800
BATCH_SIZE = 10


SourcePathMap: TypeAlias = "Dict[str, str]"
LoadedBytesMap: TypeAlias = "Dict[str, bytes]"
NumpyArrayMap: TypeAlias = "Dict[str, npt.NDArray[np.float32]]"


class DatasetInputIterable:
    def __init__(self) -> None:
        self._data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "data")

    def __iter__(self) -> Iterator[SourcePathMap]:
        return self

    def __next__(self) -> SourcePathMap:
        return {
            "input": os.path.join(self._data_dir, "semseg_input.png"),
            "label": os.path.join(self._data_dir, "semseg_label.png"),
        }


def loader(data: SourcePathMap) -> LoadedBytesMap:
    # TODO: maybe a variable buffer size would be nice here?
    input_img_bytes = Path(data["input"]).read_bytes()
    label_img_bytes = Path(data["label"]).read_bytes()
    return {"input": input_img_bytes, "label": label_img_bytes}


@functools.lru_cache
def get_png_decoder(pixel_format: PixelFormat) -> ImageDecoder:
    config = ImageDecoderConfig()
    config.enabled_decoders = [ImageDecoderType.PNG]
    config.pixel_format = pixel_format
    decoder = ImageDecoder(config)
    return decoder


def processor(data: LoadedBytesMap) -> NumpyArrayMap:
    # TODO: why is cv2.imdecode so much slower than cv2.imread? -> tmp solution is to use wuffs instead
    input_img = get_png_decoder(PixelFormat.RGB).decode(data["input"]).pixbuf
    label_img = get_png_decoder(PixelFormat.Y).decode(data["label"]).pixbuf

    max_y = input_img.shape[0] - TARGET_HEIGHT
    max_x = input_img.shape[1] - TARGET_WIDTH

    x = np.random.randint(0, max_x)
    y = np.random.randint(0, max_y)

    cropped_input_img = input_img[y : y + TARGET_HEIGHT, x : x + TARGET_WIDTH]
    cropped_label_img = label_img[y : y + TARGET_HEIGHT, x : x + TARGET_WIDTH]

    cropped_input_img = cropped_input_img.astype(np.float32)
    cropped_label_img = cropped_label_img.astype(np.float32)

    return {"input": cropped_input_img, "label": cropped_label_img}


def batcher(data: Iterable[NumpyArrayMap]) -> NumpyArrayMap:
    input_images = np.asarray([data_dict["input"] for data_dict in data])
    label_images = np.asarray([data_dict["label"] for data_dict in data])
    return {"input": input_images, "label": label_images}


def encoder(data: NumpyArrayMap) -> bytes:
    encoded_data = data["input"].tobytes() + data["label"].tobytes()
    return encoded_data


def decoder(data: bytes) -> NumpyArrayMap:
    num_img_floats = BATCH_SIZE * TARGET_WIDTH * TARGET_HEIGHT * 3
    input_images = np.frombuffer(data, dtype=np.float32, count=num_img_floats).reshape(
        (BATCH_SIZE, TARGET_WIDTH, TARGET_HEIGHT, 3)
    )
    label_images = np.frombuffer(data, dtype=np.float32, offset=num_img_floats * 4, count=-1).reshape(
        (BATCH_SIZE, TARGET_WIDTH, TARGET_HEIGHT)
    )
    return {"input": input_images, "label": label_images}


def main() -> None:
    pyper = Pyper(
        loader=loader,
        processor=processor,
        batcher=batcher,
        encoder=encoder,
        decoder=decoder,
        batch_size=BATCH_SIZE,
        num_bytes_per_batch=BATCH_SIZE * TARGET_WIDTH * TARGET_HEIGHT * 3 * 4
        + BATCH_SIZE * TARGET_WIDTH * TARGET_HEIGHT * 4,
    )

    cpu_cores = os.cpu_count()
    assert cpu_cores
    os.sched_setaffinity(0, {cpu_cores - 1})

    generator = pyper.multi_worker_generator(
        input_data_iterable=DatasetInputIterable(),
        # scaling with workers is near linear. check the following table for results from this code on a 3900x with
        # num_loader_threads_per_worker=2, batch_size=10 and f32 buffers:
        #
        # | workers             | 1     | 2     | 4     | 8      | 11     |
        # |---------------------|-------|-------|-------|--------|--------|
        # | examples per second | 15.29 | 30.15 | 58.23 | 103.30 | 120.57 |
        num_worker=1,
        num_loader_threads_per_worker=2,
    )
    # generator = pyper.debug_generator(
    #     input_data_iterable=DatasetInputIterable(),
    # )

    # while True:
    #     data_batch = next(generator)
    #     print(data_batch)

    def tensor_generator_builder() -> Generator[Dict[str, tf.Tensor], None, None]:
        while True:
            batch = next(generator)
            yield batch

    tf_dataset = tf.data.Dataset.from_generator(
        tensor_generator_builder,
        output_signature={
            "input": tf.TensorSpec(shape=(BATCH_SIZE, TARGET_WIDTH, TARGET_HEIGHT, 3), dtype=tf.float32),
            "label": tf.TensorSpec(shape=(BATCH_SIZE, TARGET_WIDTH, TARGET_HEIGHT), dtype=tf.float32),
        },
    )

    num_iterations = 100

    tfds.benchmark(tf_dataset, num_iter=num_iterations, batch_size=BATCH_SIZE)

    # step_time = 0.250
    # print(f"Starting prefetching test. A perfect result would be: {step_time * num_iterations}")
    # tf_dataset = tf_dataset.prefetch(10)
    # tf_dataset_iterator = iter(tf_dataset)
    # next(tf_dataset_iterator)
    # time.sleep(10)
    # start_time = time.perf_counter()
    # for step in range(num_iterations):
    #     start = time.time()
    #     _batch = next(tf_dataset_iterator)
    #     print(time.time() - start)
    #     time.sleep(step_time)
    # print("Execution time:", time.perf_counter() - start_time)


if __name__ == "__main__":
    main()
