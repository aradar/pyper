#!/usr/bin/env bash

set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd ${SCRIPT_DIR}

echo "calling 'ruff check --fix src/pyper examples/'"
echo "=============================================="
ruff check --fix src/pyper examples/
echo

echo "calling 'mypy --strict --package pyper'"
echo "======================================="
mypy --strict --package pyper --strict
echo

echo "calling 'mypy --strict examples/'"
echo "================================="
mypy --strict examples/
