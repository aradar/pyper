import dataclasses
import os
import queue
import multiprocessing as mp
import multiprocessing.shared_memory as mpsm
import multiprocessing.managers as mpm
import multiprocessing.synchronize as mps
import time
from collections import deque
from dataclasses import dataclass
from threading import Thread
from typing import Generic, Generator, Iterable, cast, Deque, Optional, NamedTuple, List

import dill

from pyper.step import (
    LoaderInput,
    Loader,
    LoaderOutput,
    Processor,
    ProcessorOutput,
    BatcherOutput,
    Batcher,
    Decoder,
    Encoder,
)


# TODO: implement a special thread that allows the queue state tracking and contains the endless loop and prevents
#  crashes


class _Worker(NamedTuple):
    process: mp.Process
    shm_can_be_reused: mps.Event
    shm: mpsm.SharedMemory


def generator(
    input_data_iterable: Iterable[LoaderInput],
    loader: Loader[LoaderInput, LoaderOutput],
    processor: Processor[LoaderOutput, ProcessorOutput],
    batcher: Batcher[ProcessorOutput, BatcherOutput],
    decoder: Decoder[BatcherOutput],
    encoder: Encoder[BatcherOutput],
    batch_size: int,
    num_bytes_per_batch: int,
    num_worker: int,
    num_loader_threads_per_worker: int,
    cycle_sleep_time: float,
    worker_core_pool: Optional[Iterable[int]] = None,
) -> Generator[BatcherOutput, None, None]:
    if worker_core_pool is None:
        cpu_count = os.cpu_count()
        assert cpu_count
        worker_core_pool = range(cpu_count)
    worker_core_pool = list(sorted(worker_core_pool))

    if len(worker_core_pool) < num_worker:
        raise ValueError(f"{worker_core_pool=} has less than {num_worker=} elements")

    with mpm.SyncManager() as sync_manager, mpm.SharedMemoryManager() as shm_manager:
        workers = _spawn_workers(
            input_data_iterable=input_data_iterable,
            loader=loader,
            processor=processor,
            batcher=batcher,
            encoder=encoder,
            batch_size=batch_size,
            num_bytes_per_batch=num_bytes_per_batch,
            num_worker=num_worker,
            num_loader_threads_per_worker=num_loader_threads_per_worker,
            shm_manager=shm_manager,
            sync_manager=sync_manager,
            worker_core_pool=worker_core_pool,
        )

        while True:
            for i in range(num_worker):
                # we cycle with this consistently through the number of available workers
                worker = workers.popleft()
                workers.append(worker)
                if not worker.shm_can_be_reused.is_set():
                    read_encoded_batch_data_shm = worker.shm.buf.tobytes()
                    decoded_data = decoder(read_encoded_batch_data_shm)
                    worker.shm_can_be_reused.set()
                    yield decoded_data
                    break  # we must break from this loop as otherwise it will continue the last iteration
            else:
                # we sleep here to keep the load from polling lower
                if cycle_sleep_time > 0:
                    time.sleep(cycle_sleep_time)


def _spawn_workers(
    input_data_iterable: Iterable[LoaderInput],
    loader: Loader[LoaderInput, LoaderOutput],
    processor: Processor[LoaderOutput, ProcessorOutput],
    batcher: Batcher[ProcessorOutput, BatcherOutput],
    encoder: Encoder[BatcherOutput],
    batch_size: int,
    num_bytes_per_batch: int,
    num_worker: int,
    num_loader_threads_per_worker: int,
    shm_manager: mpm.SharedMemoryManager,
    sync_manager: mpm.SyncManager,
    worker_core_pool: List[int],
) -> Deque[_Worker]:
    context = mp.get_context("spawn")
    workers: Deque[_Worker] = deque()
    for i in range(num_worker):
        shm_can_be_reused = cast(mps.Event, sync_manager.Event())
        shm_can_be_reused.set()
        shm = shm_manager.SharedMemory(size=num_bytes_per_batch)

        encoded_worker_args = _SendableWorkerArgs.encode(
            _WorkerArgs(
                input_data_iterable=input_data_iterable,
                loader=loader,
                processor=processor,
                batcher=batcher,
                encoder=encoder,
                batch_size=batch_size,
                encoded_data_shm_can_be_reused=shm_can_be_reused,
                encoded_data_shm=shm,
                num_loader_threads=num_loader_threads_per_worker,
            )
        )

        process = cast(
            mp.Process,
            context.Process(
                target=_bootstrap_worker,
                name=f"pyper_worker_{i}",
                daemon=True,
                args=(encoded_worker_args,),
            ),
        )
        process.start()
        assert process.pid
        os.sched_setaffinity(process.pid, {worker_core_pool[i]})

        workers.append(_Worker(process, shm_can_be_reused, shm))

    return workers


@dataclass
class _WorkerArgs(Generic[LoaderInput, LoaderOutput, ProcessorOutput, BatcherOutput]):
    input_data_iterable: Iterable[LoaderInput]
    loader: Loader[LoaderInput, LoaderOutput]
    processor: Processor[LoaderOutput, ProcessorOutput]
    batcher: Batcher[ProcessorOutput, BatcherOutput]
    encoder: Encoder[BatcherOutput]
    batch_size: int
    encoded_data_shm_can_be_reused: mps.Event
    encoded_data_shm: mpsm.SharedMemory
    num_loader_threads: int


@dataclass
class _SendableWorkerArgs(Generic[LoaderInput, LoaderOutput, ProcessorOutput, BatcherOutput]):
    encoded_args: bytes

    @classmethod
    def encode(
        cls,
        args: _WorkerArgs[LoaderInput, LoaderOutput, ProcessorOutput, BatcherOutput],
    ) -> "_SendableWorkerArgs[LoaderInput, LoaderOutput, ProcessorOutput, BatcherOutput]":
        kwargs = {field.name: getattr(args, field.name) for field in dataclasses.fields(args)}
        # TODO: decide if the way forward is dill or cloudpickle
        encoded_args: bytes = dill.dumps(kwargs, recurse=True)
        return cls(encoded_args)

    def decode(
        self,
    ) -> "_WorkerArgs[LoaderInput, LoaderOutput, ProcessorOutput, BatcherOutput]":
        kwargs = dill.loads(self.encoded_args)
        return _WorkerArgs(**kwargs)


def _bootstrap_worker(
    encoded_worker_args: _SendableWorkerArgs[LoaderInput, LoaderOutput, ProcessorOutput, BatcherOutput],
) -> None:
    worker_args = encoded_worker_args.decode()
    _start_worker(worker_args)


def _start_worker(
    worker_args: _WorkerArgs[LoaderInput, LoaderOutput, ProcessorOutput, BatcherOutput],
) -> None:
    loaded_data_queue: "queue.Queue[LoaderOutput]" = queue.Queue(worker_args.batch_size)
    batched_data_queue: "queue.Queue[BatcherOutput]" = queue.Queue(2)

    def loader_target() -> None:
        input_iterator = iter(worker_args.input_data_iterable)
        while True:
            # TODO: how should this indicate that an iterator is exhausted?
            try:
                loaded_data = worker_args.loader(next(input_iterator))
                loaded_data_queue.put(loaded_data)
            except Exception:
                # TODO: add better error handling
                print("loader ran into an error")

    def processor_and_batcher_target() -> None:
        batch_buffer = []
        while True:
            try:
                loaded_data = loaded_data_queue.get()
                processed_data = worker_args.processor(loaded_data)

                batch_buffer.append(processed_data)
                while len(batch_buffer) >= worker_args.batch_size:
                    batched_data = worker_args.batcher(batch_buffer)
                    batched_data_queue.put(batched_data)
                    batch_buffer.clear()
            except Exception:
                # TODO: add better error handling
                print("processor or batcher ran into an error")

    def encoder_target() -> None:
        while True:
            try:
                batched_data = batched_data_queue.get()
                encoded_data = worker_args.encoder(batched_data)

                while True:
                    buffer_can_be_reused = worker_args.encoded_data_shm_can_be_reused.wait()

                    if buffer_can_be_reused:
                        worker_args.encoded_data_shm.buf[:] = encoded_data
                        worker_args.encoded_data_shm_can_be_reused.clear()
                        break
            except Exception:
                # TODO: add better error handling
                print("encoder ran into an error")

    threads = [
        *[
            Thread(name=f"pyper_loader_{i}", target=loader_target, daemon=True)
            for i in range(worker_args.num_loader_threads)
        ],
        Thread(
            name="pyper_processor_and_batcher",
            target=processor_and_batcher_target,
            daemon=True,
        ),
        Thread(name="pyper_encoder", target=encoder_target, daemon=True),
    ]

    for thread in threads:
        thread.start()

    while True:
        for thread in threads:
            if not thread.is_alive():
                print(
                    f"Oh no! One of my beloved child threads died ({thread.name}). I will now also exit this cruel "
                    f"world!"
                )
                exit(1)
