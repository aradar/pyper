import multiprocessing.managers as mpm
from typing import Generator, Iterable, List

from pyper.step import (
    LoaderInput,
    Loader,
    Processor,
    Batcher,
    Decoder,
    Encoder,
    BatcherOutput,
    LoaderOutput,
    ProcessorOutput,
)


def generator(
    input_data_iterable: Iterable[LoaderInput],
    loader: Loader[LoaderInput, LoaderOutput],
    processor: Processor[LoaderOutput, ProcessorOutput],
    batcher: Batcher[ProcessorOutput, BatcherOutput],
    decoder: Decoder[BatcherOutput],
    encoder: Encoder[BatcherOutput],
    batch_size: int,
    num_bytes_per_batch: int,
) -> Generator[BatcherOutput, None, None]:
    with mpm.SyncManager() as sync_manager, mpm.SharedMemoryManager() as shm_manager:
        input_iterator = iter(input_data_iterable)
        batch_buffer: List[ProcessorOutput] = []
        encoded_data_shm_can_be_reused = sync_manager.Event()
        encoded_data_shm_can_be_reused.set()
        encoded_data_shm = shm_manager.SharedMemory(size=num_bytes_per_batch)

        while True:
            while not len(batch_buffer) >= batch_size:
                loaded_data = loader(next(input_iterator))
                processed_data = processor(loaded_data)
                batch_buffer.append(processed_data)

            batched_data = batcher(batch_buffer)
            batch_buffer.clear()

            # this is just a sanity check for the debug generator that this works as expected
            encoded_data_shm_can_be_reused.wait(1)
            encoded_data = encoder(batched_data)
            encoded_data_shm.buf[:] = encoded_data
            encoded_data_shm_can_be_reused.clear()

            read_encoded_batch_data_shm = encoded_data_shm.buf.tobytes()
            decoded_data = decoder(read_encoded_batch_data_shm)
            encoded_data_shm_can_be_reused.set()

            yield decoded_data
