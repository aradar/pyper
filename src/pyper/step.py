from typing import TypeVar, Iterable, Callable

from typing_extensions import TypeAlias

LoaderInput = TypeVar("LoaderInput")
LoaderOutput = TypeVar("LoaderOutput")
ProcessorOutput = TypeVar("ProcessorOutput")
BatcherOutput = TypeVar("BatcherOutput")

# named according to PEP-484 (https://peps.python.org/pep-0484/#covariance-and-contravariance) and fulfils: "be liberal
# in what you accept and conservative in what you produce."
# (https://en.wikipedia.org/wiki/Covariance_and_contravariance_%28computer_science%29#Function_types)
I_contra = TypeVar("I_contra", contravariant=True)
O_co = TypeVar("O_co", covariant=True)


# we use here simple callable aliases as mypy can't recognize callback protocols with generics as of the time of writing
# and the naming of the input param is not important
Loader: TypeAlias = Callable[[I_contra], O_co]
Processor: TypeAlias = Callable[[I_contra], O_co]
Batcher: TypeAlias = Callable[[Iterable[I_contra]], O_co]
Encoder: TypeAlias = Callable[[I_contra], bytes]
Decoder: TypeAlias = Callable[[bytes], O_co]
